package com.example.androidtest.ndk;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.androidtest.R;

public class TestNDKActivity extends Activity{
	static {
		System.loadLibrary("AndroidTest");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ndk_test);
		String string = JniClient.AddStr("我是A", "我是B");
		int i = JniClient.AddInt(1, 9);
		TextView tv = (TextView)findViewById(R.id.tv);
		tv.setText(string+"  count："+i);
//		Toast.makeText(getApplicationContext(), string+"  count："+i, Toast.LENGTH_SHORT).show();
	}
	
}
