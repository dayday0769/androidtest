package com.example.androidtest;
import com.example.androidtest.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.ArrayMap;

/**
 * 类似haspmap的不重复集合
 * @author chenjianjun
 * @time 2014-11-3 上午10:44:06
 */
public class ArrayMapTest extends Activity{
	
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		ArrayMap<String, Integer> map = new ArrayMap<String, Integer>();
		map.put("a", 1);
		map.put("a", 2);
		System.out.println(map.get("a"));
	}

}
