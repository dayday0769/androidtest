#include "com_example_androidtest_ndk_JniClient.h"
#include <stdlib.h>
#include <stdio.h>

#ifdef __cplusplus   //cpp中的自定义宏，那么定义了这个宏的话表示这是一段cpp的代码
extern "C" //告诉编译器:这是一个用C写成的库文件，请用C的方式来链接它们
{
#endif

/**
 * Class: com_example_androidtest_ndk_JniClient
 * Method: Addstr
 * Signature: (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_example_androidtest_ndk_JniClient_AddStr(
		JNIEnv *env, jclass arg, jstring instringA, jstring instringB) {

	jstring cstring = (*env)->NewStringUTF(env, "HI from JNI! A:");
	jstring dstring = (*env)->NewStringUTF(env, " And B:");
	const jbyte *abt = (*env)->GetStringUTFChars(env, instringA, 0);
	const jbyte *bbt = (*env)->GetStringUTFChars(env, instringB, 0);
	const jbyte *cbt = (*env)->GetStringUTFChars(env, cstring, 0);
	const jbyte *dbt = (*env)->GetStringUTFChars(env, dstring, 0);

	char* sall = malloc(strlen(abt)+strlen(bbt)+strlen(cbt)+strlen(dbt)+1);
	strcpy(sall, cbt);
	strcat(sall, abt);
	strcat(sall, dbt);
	strcat(sall, bbt);
	jstring str = (*env)->NewStringUTF(env, sall);
	(*env)->ReleaseStringUTFChars(env, instringA, abt);
	(*env)->ReleaseStringUTFChars(env, instringB, bbt);
	(*env)->ReleaseStringUTFChars(env, cstring, cbt);
	(*env)->ReleaseStringUTFChars(env, dstring, dbt);
	free(sall);
	return str;
}

/*
 * Class:     com_example_androidtest_ndk_JniClient
 * Method:    AddInt
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_com_example_androidtest_ndk_JniClient_AddInt(
		JNIEnv *env, jclass arg, jint a, jint b) {
	return a + b;
}

#ifdef __cplusplus
}
#endif
